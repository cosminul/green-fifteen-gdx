package ro.hume.cosmin.fifteen.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import ro.hume.cosmin.fifteen.FifteenGame

object DesktopLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = 960
        config.height = 540
        LwjglApplication(FifteenGame(), config)
    }
}
