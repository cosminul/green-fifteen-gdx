package ro.hume.cosmin.fifteen.logic

data class Position(var line: Int, var column: Int) {

    override fun toString(): String {
        return "($line, $column)"
    }
}
