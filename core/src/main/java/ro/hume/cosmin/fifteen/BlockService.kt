package ro.hume.cosmin.fifteen

import java.util.*

class BlockService {

    private val blocks: MutableMap<Int, Block> = HashMap()

    fun addBlock(block: Block) {
        blocks[block.code] = block
    }

    fun getBlockForCode(code: Int): Block {
        return blocks[code] ?: Block.EMPTY
    }
}
