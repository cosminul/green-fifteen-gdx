package ro.hume.cosmin.fifteen.logic

import java.util.*

class Shuffler(private val fifteen: Fifteen) {

    private var positionOfZero = fifteen.positionOfZero

    fun shuffle() {
        positionOfZero = fifteen.positionOfZero
        val touchablePositions = touchablePositions
        val random = Random()
        val randomPositionIndex = random.nextInt(touchablePositions.size)
        val randomPosition = touchablePositions[randomPositionIndex]
        fifteen.touch(randomPosition)
    }

    private val touchablePositions: List<Position>
        get() {
            val touchablePositions: MutableList<Position> = LinkedList()
            touchablePositions.addAll(touchablePositionsOnLine)
            touchablePositions.addAll(touchableLinesOnColumn)
            return touchablePositions
        }

    private val touchablePositionsOnLine: List<Position>
        get() {
            val touchablePositions: MutableList<Position> = LinkedList()
            for (column in 0 until fifteen.size()) {
                if (column != positionOfZero.column) {
                    val touchablePosition = Position(
                            positionOfZero.line, column)
                    touchablePositions.add(touchablePosition)
                }
            }
            return touchablePositions
        }

    private val touchableLinesOnColumn: List<Position>
        get() {
            val touchablePositions: MutableList<Position> = LinkedList()
            for (line in 0 until fifteen.size()) {
                if (line != positionOfZero.line) {
                    val touchablePosition = Position(line,
                            positionOfZero.column)
                    touchablePositions.add(touchablePosition)
                }
            }
            return touchablePositions
        }
}
