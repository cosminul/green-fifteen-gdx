package ro.hume.cosmin.fifteen

class Block {
    var code = 0
    var spriteX = 0
    var spriteY = 0

    companion object {
        val EMPTY = Block()
    }
}
