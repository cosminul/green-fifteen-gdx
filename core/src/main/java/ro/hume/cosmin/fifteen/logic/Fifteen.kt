package ro.hume.cosmin.fifteen.logic

class Fifteen {

    companion object {
        const val boardSize = 4
        private val SOLVED = Fifteen()
    }

    var blocks: Array<IntArray>
        private set
    val positionOfZero = Position(-1, -1)

    constructor() {
        blocks = Array(boardSize) { IntArray(boardSize) }
        var index = 0
        for (line in 0 until boardSize) {
            for (column in 0 until boardSize) {
                index++
                blocks[line][column] = index
            }
        }
        blocks[boardSize - 1][boardSize - 1] = 0
        findOutPositionOfZero()
    }

    constructor(board: Array<IntArray>) {
        blocks = Array(boardSize) { IntArray(boardSize) }
        for (line in 0 until boardSize) {
            for (column in 0 until boardSize) {
                blocks[line][column] = board[line][column]
            }
        }
        findOutPositionOfZero()
    }

    fun touch(position: Position) {
        touch(position.line, position.column)
    }

    fun touch(line: Int, column: Int) {
        validateLineIndex(line)
        validateColumnIndex(column)
        val zeroColumn = positionOfZeroOnLine(line)
        val zeroLine = positionOfZeroOnColumn(column)
        if (zeroColumn != -1) {
            moveBlocksOnLine(line, column, zeroColumn)
        } else if (zeroLine != -1) {
            moveBlocksOnColumn(line, column, zeroLine)
        }
        findOutPositionOfZero()
    }

    private fun positionOfZeroOnLine(line: Int): Int {
        var columnOfZero = -1
        if (positionOfZero.line == line) {
            columnOfZero = positionOfZero.column
        }
        return columnOfZero
    }

    private fun positionOfZeroOnColumn(column: Int): Int {
        var lineOfZero = -1
        if (positionOfZero.column == column) {
            lineOfZero = positionOfZero.line
        }
        return lineOfZero
    }

    private fun validateLineIndex(line: Int) {
        require(!(line < 0 || line > boardSize - 1)) {
            String.format(
                    "Line index out of bounds (0-%d).", boardSize - 1)
        }
    }

    private fun validateColumnIndex(column: Int) {
        require(!(column < 0 || column > boardSize - 1)) {
            String.format(
                    "Column index out of bounds (0-%d).", boardSize - 1)
        }
    }

    private fun moveBlocksOnLine(touchLine: Int, touchColumn: Int, zeroColumn: Int) {
        if (touchColumn < zeroColumn) {
            for (j in zeroColumn downTo touchColumn + 1) {
                blocks[touchLine][j] = blocks[touchLine][j - 1]
            }
        } else {
            for (j in zeroColumn until touchColumn) {
                blocks[touchLine][j] = blocks[touchLine][j + 1]
            }
        }
        blocks[touchLine][touchColumn] = 0
    }

    private fun moveBlocksOnColumn(touchLine: Int, touchColumn: Int, zeroLine: Int) {
        if (touchLine < zeroLine) {
            for (i in zeroLine downTo touchLine + 1) {
                blocks[i][touchColumn] = blocks[i - 1][touchColumn]
            }
        } else {
            for (i in zeroLine until touchLine) {
                blocks[i][touchColumn] = blocks[i + 1][touchColumn]
            }
        }
        blocks[touchLine][touchColumn] = 0
    }

    private fun findOutPositionOfZero() {
        for (line in 0 until boardSize) {
            for (column in 0 until boardSize) {
                if (blocks[line][column] == 0) {
                    positionOfZero.line = line
                    positionOfZero.column = column
                }
            }
        }
    }

    val isSolved: Boolean
        get() = blocks.contentDeepEquals(SOLVED.blocks)

    fun size() = boardSize
}
