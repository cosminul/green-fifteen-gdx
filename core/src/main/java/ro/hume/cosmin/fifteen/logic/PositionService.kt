package ro.hume.cosmin.fifteen.logic

class PositionService(private val worldWidth: Int, private val worldHeight: Int) {

    fun translatePosition(worldX: Float, worldY: Float): Position {
        return Position((4 - 1 - worldY.toInt() * 4 / worldHeight), (worldX.toInt() * 4 / worldWidth))
    }
}
