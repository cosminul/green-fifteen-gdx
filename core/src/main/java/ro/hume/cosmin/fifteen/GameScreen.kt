package ro.hume.cosmin.fifteen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import ro.hume.cosmin.fifteen.logic.Fifteen
import ro.hume.cosmin.fifteen.logic.PositionService
import ro.hume.cosmin.fifteen.logic.Shuffler

class GameScreen(private val game: FifteenGame) : ScreenAdapter() {

    companion object {
        private const val SIZE = 1080
        private const val BOARD_PADDING = 5
        private const val BLOCK_MARGIN = 5
        private const val BLOCKS_PER_ROW = 4
        private const val BLOCK_SIZE = ((SIZE - ((BLOCKS_PER_ROW - 1)
                * BLOCK_MARGIN) - 2 * BOARD_PADDING)
                / BLOCKS_PER_ROW)
    }

    private val camera: OrthographicCamera = OrthographicCamera()
    private val viewport: Viewport
    private val allNumbers: Texture
    private val fifteen: Fifteen
    private val shuffler: Shuffler
    private val blockService: BlockService
    private val positionService: PositionService

    init {
        camera.setToOrtho(false, SIZE.toFloat(), SIZE.toFloat())
        viewport = FitViewport(SIZE.toFloat(), SIZE.toFloat(), camera)
        allNumbers = Texture(Gdx.files.internal("digits_1080.png"))
        fifteen = Fifteen()
        shuffler = Shuffler(fifteen)
        blockService = BlockService()
        positionService = PositionService(SIZE, SIZE)
        initBlocks()
        shuffleBoard()
    }

    private fun initBlocks() {
        val fifteenBlocks = fifteen.blocks
        for (line in 0 until BLOCKS_PER_ROW) {
            for (column in 0 until BLOCKS_PER_ROW) {
                val fileX = (column * (BLOCK_SIZE + BLOCK_MARGIN)
                        + BOARD_PADDING)
                val fileY = line * (BLOCK_SIZE + BLOCK_MARGIN) + BOARD_PADDING
                val code = fifteenBlocks[line][column]
                var block: Block
                if (code == 0) {
                    block = Block.EMPTY
                } else {
                    block = Block()
                    block.code = code
                    block.spriteX = fileX
                    block.spriteY = fileY
                }
                blockService.addBlock(block)
            }
        }
    }

    private fun shuffleBoard() {
        for (i in 0..99) {
            shuffler.shuffle()
        }
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0.2f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined
        game.batch.begin()
        for (line in 0 until BLOCKS_PER_ROW) {
            for (column in 0 until BLOCKS_PER_ROW) {
                drawBlock(line, column)
            }
        }
        game.batch.end()
        handleTouches()
    }

    private fun drawBlock(line: Int, column: Int) {
        val block = blockService
                .getBlockForCode(fifteen.blocks[line][column])
        if (block !== Block.EMPTY) {
            val fileX = column * (BLOCK_SIZE + BLOCK_MARGIN) + BOARD_PADDING
            val fileY = line * (BLOCK_SIZE + BLOCK_MARGIN) + BOARD_PADDING
            val screenY = SIZE - fileY - BLOCK_SIZE
            game.batch.draw(allNumbers, fileX.toFloat(), screenY.toFloat(), BLOCK_SIZE.toFloat(),
                    BLOCK_SIZE.toFloat(), block.spriteX, block.spriteY, BLOCK_SIZE,
                    BLOCK_SIZE, false, false)
        }
    }

    private fun handleTouches() {
        if (Gdx.input.justTouched()) {
            val touchPos = Vector3()
            touchPos[Gdx.input.x.toFloat(), Gdx.input.y.toFloat()] = 0f
            viewport.unproject(touchPos)
            fifteen.touch(positionService.translatePosition(touchPos.x,
                    touchPos.y))
            if (fifteen.isSolved) {
                game.screen = GameOverScreen(game)
                dispose()
            }
        }
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
    }
}
