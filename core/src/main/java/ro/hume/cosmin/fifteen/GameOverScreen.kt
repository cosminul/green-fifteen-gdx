package ro.hume.cosmin.fifteen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.GlyphLayout

class GameOverScreen(private val game: FifteenGame) : ScreenAdapter() {

    companion object {
        private const val SIZE = 540
    }

    private val camera: OrthographicCamera = OrthographicCamera()

    private val layout = GlyphLayout()

    init {
        camera.setToOrtho(false, SIZE.toFloat(), SIZE.toFloat())
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0.2f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined
        game.batch.begin()
        layout.setText(game.font, "You did it!")
        val text1X = SIZE / 2 - layout.width / 2
        val text1Y = SIZE * 3 / 4 - layout.height / 2
        game.font.draw(game.batch, "You did it!", text1X, text1Y)
        layout.setText(game.font, "Tap anywhere to play again.")
        val text2X = SIZE / 2 - layout.width / 2
        val text2Y = SIZE / 2 - layout.height / 2
        game.font.draw(game.batch, "Tap anywhere to play again.", text2X, text2Y)
        game.batch.end()
        if (Gdx.input.justTouched()) {
            game.screen = GameScreen(game)
            dispose()
        }
    }
}
