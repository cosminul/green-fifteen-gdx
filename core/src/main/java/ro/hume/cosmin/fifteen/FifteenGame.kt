package ro.hume.cosmin.fifteen

import com.badlogic.gdx.Game
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch

class FifteenGame : Game() {

    lateinit var batch: SpriteBatch
    lateinit var font: BitmapFont

    override fun create() {
        batch = SpriteBatch()
        font = BitmapFont()
        screen = GameScreen(this)
    }

    override fun dispose() {
        font.dispose()
        batch.dispose()
    }
}
