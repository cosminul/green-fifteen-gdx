package ro.hume.cosmin.fifteen.logic

import org.junit.Test

class FifteenHorizontalTest : FifteenTest() {

    @Test
    fun testTouch_0_1_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(0, 1)
        assertLine(0, intArrayOf(1, 0, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_2_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(0, 2)
        assertLine(0, intArrayOf(1, 2, 0, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_3_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(0, 3)
        assertLine(0, intArrayOf(1, 2, 3, 0))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_0_1() {
        initWithPositionOfZero(0, 1)
        touchAtPosition(0, 0)
        assertLine(0, intArrayOf(0, 1, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_2_pivot_0_1() {
        initWithPositionOfZero(0, 1)
        touchAtPosition(0, 2)
        assertLine(0, intArrayOf(1, 2, 0, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_3_pivot_0_1() {
        initWithPositionOfZero(0, 1)
        touchAtPosition(0, 3)
        assertLine(0, intArrayOf(1, 2, 3, 0))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_0_2() {
        initWithPositionOfZero(0, 2)
        touchAtPosition(0, 0)
        assertLine(0, intArrayOf(0, 1, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_1_pivot_0_2() {
        initWithPositionOfZero(0, 2)
        touchAtPosition(0, 1)
        assertLine(0, intArrayOf(1, 0, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_3_pivot_0_2() {
        initWithPositionOfZero(0, 2)
        touchAtPosition(0, 3)
        assertLine(0, intArrayOf(1, 2, 3, 0))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_0_3() {
        initWithPositionOfZero(0, 3)
        touchAtPosition(0, 0)
        assertLine(0, intArrayOf(0, 1, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_1_pivot_0_3() {
        initWithPositionOfZero(0, 3)
        touchAtPosition(0, 1)
        assertLine(0, intArrayOf(1, 0, 2, 3))
        assertOtherLinesNotChanged()
    }

    @Test
    fun testTouch_0_2_pivot_0_3() {
        initWithPositionOfZero(0, 3)
        touchAtPosition(0, 2)
        assertLine(0, intArrayOf(1, 2, 0, 3))
        assertOtherLinesNotChanged()
    }

    private fun assertOtherLinesNotChanged() {
        assertLineNotChanged(1)
        assertLineNotChanged(2)
        assertLineNotChanged(3)
    }
}
