package ro.hume.cosmin.fifteen.logic

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PositionServiceTest {

    private var positionService: PositionService? = null

    @Before
    fun setUp() {
        positionService = PositionService(WORLD_SIZE, WORLD_SIZE)
    }

    @Test
    fun testLine0Col0() {
        val pos = positionService!!.translatePosition(20f, 200f)
        Assert.assertEquals(0, pos.line.toLong())
        Assert.assertEquals(0, pos.column.toLong())
    }

    @Test
    fun testLine1Col0() {
        val pos = positionService!!.translatePosition(20f, 160f)
        Assert.assertEquals(1, pos.line.toLong())
        Assert.assertEquals(0, pos.column.toLong())
    }

    @Test
    fun testLine2Col0() {
        val pos = positionService!!.translatePosition(20f, 100f)
        Assert.assertEquals(2, pos.line.toLong())
        Assert.assertEquals(0, pos.column.toLong())
    }

    @Test
    fun testLine3Col0() {
        val pos = positionService!!.translatePosition(20f, 20f)
        Assert.assertEquals(3, pos.line.toLong())
        Assert.assertEquals(0, pos.column.toLong())
    }

    @Test
    fun testLine3Col1() {
        val pos = positionService!!.translatePosition(100f, 20f)
        Assert.assertEquals(3, pos.line.toLong())
        Assert.assertEquals(1, pos.column.toLong())
    }

    @Test
    fun testLine3Col2() {
        val pos = positionService!!.translatePosition(160f, 20f)
        Assert.assertEquals(3, pos.line.toLong())
        Assert.assertEquals(2, pos.column.toLong())
    }

    @Test
    fun testLine3Col3() {
        val pos = positionService!!.translatePosition(200f, 20f)
        Assert.assertEquals(3, pos.line.toLong())
        Assert.assertEquals(3, pos.column.toLong())
    }

    companion object {
        private const val WORLD_SIZE = 240
    }
}
