package ro.hume.cosmin.fifteen.logic

import org.junit.Assert
import org.junit.Before

abstract class FifteenTest {

    companion object {
        private const val BOARD_SIZE = 4
    }

    private lateinit var fifteen: Fifteen
    private lateinit var shuffler: Shuffler
    private lateinit var boardPrinter: BoardPrinter
    private lateinit var before: Array<IntArray>
    private lateinit var after: Array<IntArray>

    @Before
    fun setUp() {
        fifteen = Fifteen()
        shuffler = Shuffler(fifteen)
        boardPrinter = BoardPrinter()
    }

    protected fun initWithPositionOfZero(lineOfZero: Int, columnOfZero: Int) {
        before = generate(lineOfZero, columnOfZero)
        fifteen = Fifteen(before)
        boardPrinter.print(fifteen)
    }

    // TODO refactor, this is only used for touch tests
    protected fun touchAtPosition(lineOfTouch: Int, columnOfTouch: Int) {
        fifteen.touch(lineOfTouch, columnOfTouch)
        after = fifteen.blocks
        boardPrinter.print(fifteen)
    }

    // TODO refactor, this is only used for shuffle tests
    protected fun shuffle() {
        boardPrinter.print(fifteen)
        shuffler.shuffle()
        boardPrinter.print(fifteen)
    }

    private fun generate(lineOfZero: Int, columnOfZero: Int): Array<IntArray> {
        val blocks: Array<IntArray> = Array(BOARD_SIZE) { IntArray(BOARD_SIZE) }
        var index = 0
        for (line in 0 until BOARD_SIZE) {
            for (column in 0 until BOARD_SIZE) {
                if (line == lineOfZero && column == columnOfZero) {
                    blocks[line][column] = 0
                } else {
                    index++
                    blocks[line][column] = index
                }
            }
        }
        blocks[lineOfZero][columnOfZero] = 0
        return blocks
    }

    protected fun assertColumnNotChanged(column: Int) {
        for (line in 0 until BOARD_SIZE) {
            Assert.assertEquals(before[line][column], after[line][column])
        }
    }

    protected fun assertLineNotChanged(line: Int) {
        for (column in 0 until BOARD_SIZE) {
            Assert.assertEquals(before[line][column], after[line][column])
        }
    }

    protected fun assertLine(lineIndex: Int, expectedValues: IntArray?) {
        Assert.assertArrayEquals(expectedValues, after[lineIndex])
    }

    protected fun assertColumn(columnIndex: Int, expectedValues: IntArray) {
        for (line in 0 until BOARD_SIZE) {
            Assert.assertEquals(expectedValues[line], after[line][columnIndex])
        }
    }
}
