package ro.hume.cosmin.fifteen.logic

class BoardPrinter {

    fun print(board: Fifteen) {
        val boardSize: Int = board.size()
        val blocks = board.blocks
        for (line in 0 until boardSize) {
            for (column in 0 until boardSize) {
                System.out.format("%2d", blocks[line][column])
                print(" ")
            }
            println()
        }
    }
}
