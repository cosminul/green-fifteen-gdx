package ro.hume.cosmin.fifteen.logic

import org.junit.Test

class FifteenVerticalTest : FifteenTest() {

    @Test
    fun testTouch_1_0_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(1, 0)
        assertColumn(0, intArrayOf(4, 0, 8, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_2_0_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(2, 0)
        assertColumn(0, intArrayOf(4, 8, 0, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_3_0_pivot_0_0() {
        initWithPositionOfZero(0, 0)
        touchAtPosition(3, 0)
        assertColumn(0, intArrayOf(4, 8, 12, 0))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_1_0() {
        initWithPositionOfZero(1, 0)
        touchAtPosition(0, 0)
        assertColumn(0, intArrayOf(0, 1, 8, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_2_0_pivot_1_0() {
        initWithPositionOfZero(1, 0)
        touchAtPosition(2, 0)
        assertColumn(0, intArrayOf(1, 8, 0, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_3_0_pivot_1_0() {
        initWithPositionOfZero(1, 0)
        touchAtPosition(3, 0)
        assertColumn(0, intArrayOf(1, 8, 12, 0))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_2_0() {
        initWithPositionOfZero(2, 0)
        touchAtPosition(0, 0)
        assertColumn(0, intArrayOf(0, 1, 5, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_1_0_pivot_2_0() {
        initWithPositionOfZero(2, 0)
        touchAtPosition(1, 0)
        assertColumn(0, intArrayOf(1, 0, 5, 12))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_3_0_pivot_2_0() {
        initWithPositionOfZero(2, 0)
        touchAtPosition(3, 0)
        assertColumn(0, intArrayOf(1, 5, 12, 0))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_0_0_pivot_3_0() {
        initWithPositionOfZero(3, 0)
        touchAtPosition(0, 0)
        assertColumn(0, intArrayOf(0, 1, 5, 9))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_1_0_pivot_3_0() {
        initWithPositionOfZero(3, 0)
        touchAtPosition(1, 0)
        assertColumn(0, intArrayOf(1, 0, 5, 9))
        assertOtherColumnsNotChanged()
    }

    @Test
    fun testTouch_2_0_pivot_3_0() {
        initWithPositionOfZero(3, 0)
        touchAtPosition(2, 0)
        assertColumn(0, intArrayOf(1, 5, 0, 9))
        assertOtherColumnsNotChanged()
    }

    private fun assertOtherColumnsNotChanged() {
        assertColumnNotChanged(1)
        assertColumnNotChanged(2)
        assertColumnNotChanged(3)
    }
}
